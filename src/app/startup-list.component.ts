import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router }            from '@angular/router';
import { Angular2Apollo } from 'angular2-apollo';
import { Subscription } from 'rxjs/Subscription';

import gql from 'graphql-tag';

const AllStartupsQuery= gql`
  query allStartups {
      allStartups {
          id
          name
          creationDate
          categories {
            name
          }
          funds {
            nbActions
            actions {
              nbAction
            }
            orders(filter:{orderType:"Sale"}) {
              nbAction
            }
          }
      }
  }
`;

@Component({
  selector: 'startup-list',
  templateUrl: 'startup-list.component.html'
})
export class StartupListComponent implements OnInit, OnDestroy {

  loading: boolean = true;
  allStartups: any;
  allStartupsSub: Subscription;

  constructor(
    private apollo: Angular2Apollo
  ) {}

  ngOnInit() {
    this.allStartupsSub = this.apollo.watchQuery({
      query: AllStartupsQuery,
      pollInterval: 10000,
    }).subscribe(({data, loading}) => {
      this.allStartups = data.allStartups.reverse();
      this.loading = loading;
    });
  }

  ngOnDestroy() {
    this.allStartupsSub.unsubscribe();
  }

  isBuyAvailable(startup:any) {
    for (let fund of startup.funds) {
      let total = fund.nbActions;
      let totalSale = 0;

      for (let action of fund.actions) {
         totalSale += action.nbAction;
      }

      let percent = totalSale / total;
      if (percent > 0 && percent < 100) {
        return true;
      }
      return false;
    }
  }

  isSaleAvailable(startup:any) {
    for (let fund of startup.funds) {
      for (let order of fund.orders) {
        if (order.nbAction > 0) {
          return true;
        }
      }
    }
    return false;
  }

}
