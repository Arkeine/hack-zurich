import { Component, OnInit, OnDestroy, HostBinding,
         trigger, transition, animate,
         style, state } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Angular2Apollo } from 'angular2-apollo';
import { Subscription } from 'rxjs/Subscription';

import gql from 'graphql-tag';

const investorQuery = gql`
  query investor ($id: ID!) {
      Investor(id: $id) {
          name
          actions {
            nbAction
            fund {
              amount
              description
              nbActions
              startup {
                id
                name
              }
              orders(filter:{orderType:"Sale"}) {
                directPrice
              }
            }
          }
      }
  }
`;

@Component({
  selector: 'dashboard-investor',
  templateUrl: 'dashboard-investor.component.html'
})
export class DashboardInvestorComponent implements OnInit, OnDestroy {

  investorId: string = "cit6f3hg701qk0131hwue6g14"; // Default investor (no login right now)
  loading: boolean = true;
  investor: any;
  investorSub: Subscription;

  constructor(
      private apollo: Angular2Apollo,
      private route: ActivatedRoute,
      private router: Router) {}

  ngOnInit() {
    this.investorSub = this.apollo.watchQuery({
       query: investorQuery,
       variables: {
         id: this.investorId
       },
       pollInterval: 10000,
     }).subscribe(({data, loading}) => {
       this.investor = data.Investor;
       console.log("RESULT: ", this.investor);
       this.loading = loading;
     });
  }

  ngOnDestroy() {
    this.investorSub.unsubscribe();
  }

  currentRetailPrice(action:any) {
    let numberSale = 0;
    let priceSale = 0;

    for (let order of action.fund.orders) {
      numberSale += 1;
      priceSale += order.directPrice;
    }

    if (numberSale == 0) {
      return "Currently no sales"
    }

    return Math.round(priceSale / numberSale) + "$";
  }

  sell(id) {
    this.router.navigate(['startup-invest', id]);
  }

}
