import { NgModule } from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { RouterModule } from '@angular/router';
import { ApolloModule } from 'angular2-apollo';

import { AppComponent } from './app.component';
import { HomeComponent }              from './home.component';
import { DashboardStartupComponent }  from './dashboard-startup.component';
import { DashboardInvestorComponent } from './dashboard-investor.component';
import { StartupListComponent }       from './startup-list.component';
import { StartupDetailsComponent }    from './startup-details.component';
import { StartupEditComponent }       from './startup-edit.component';
import { StartupInvestComponent }     from './startup-invest.component';
import { AboutComponent }             from './about.component';
//import { routes } from './routes';
import { routing }          from './app.routing';
import { client } from './client';

//import { ActionService }    from './actions.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardStartupComponent,
    DashboardInvestorComponent,
    StartupListComponent,
    StartupDetailsComponent,
    StartupEditComponent,
    StartupInvestComponent,
    AboutComponent
  ],
  entryComponents: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    //ReactiveFormsModule,
    //RouterModule.forRoot(routes),
    routing,
    ApolloModule.withClient(client)
  ],
  bootstrap: [ AppComponent ],
})
export class AppModule {}
