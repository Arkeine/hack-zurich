import { Component, OnInit, OnDestroy, HostBinding,
         trigger, transition, animate,
         style, state } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Angular2Apollo } from 'angular2-apollo';
import { Subscription } from 'rxjs/Subscription';

import gql from 'graphql-tag';

const startupQuery = gql`
  query startup ($id: ID!) {
      Startup(id: $id) {
          name
          image
          categories {
            name
          }
          companyDescription
          workDescription
          website
          mail
          phone
          creationDate
          facebook
          linkedin
          twitter
          funds {
            id
            amount
            nbActions
            actions {
              nbAction
            }
            orders {
              nbAction
              orderType
            }
          }
      }
  }
`;

@Component({
  selector: 'startup-details',
  templateUrl: 'startup-details.component.html'
})
export class StartupDetailsComponent implements OnInit, OnDestroy {

  investorId: string = "cit6f3hg701qk0131hwue6g14"; // Default investor (no login right now)
  nbPart: number;
  fundId: string;

  id: string;
  loading: boolean = true;
  startup: any;
  startupSub: Subscription;

  constructor(
      private apollo: Angular2Apollo,
      private route: ActivatedRoute,
      private router: Router) {}

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
       this.id = params['id'];
       this.startupSub = this.apollo.watchQuery({
          query: startupQuery,
          variables: { id: this.id },
          pollInterval: 2000,
        }).subscribe(({data, loading}) => {
          this.startup = data.Startup;
          this.loading = loading;
        });
     });
  }

  ngOnDestroy() {
    this.startupSub.unsubscribe();
  }

  availableToBuy(fund:any) {
    let total = fund.nbActions;
    let totalSale = 0;

    for (let action of fund.actions) {
       totalSale += action.nbAction;
    }

    let percent = totalSale / total;
    return 100 - Math.round(percent * 100);
  }

  currentlySelling(fund:any) {
    let total = fund.nbActions;
    let totalSale = 0;

    for (let action of fund.orders) {
      if (action.orderType == "Sale") {
        totalSale += action.nbAction;
      }
    }

    let percent = totalSale / total;
    return Math.round(percent * 100);
  }

  partPrice(fund:any) {
    return Math.round(fund.amount / fund.nbActions);
  }

  invest(values:any): void {
    this.router.navigate(['startup-invest', this.id]);
  }

}
