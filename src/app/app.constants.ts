import { Injectable } from '@angular/core';
 
@Injectable()
export class Configuration {
    public server: string = "http://foaas.com";
    public api: string = "/";
    public serverWithApi = this.server + this.api;
}