import { ModuleWithProviders }        from '@angular/core';
import { Routes, RouterModule }       from '@angular/router';

import { HomeComponent }              from './home.component';
import { DashboardStartupComponent }  from './dashboard-startup.component';
import { DashboardInvestorComponent } from './dashboard-investor.component';
import { StartupListComponent }       from './startup-list.component';
import { StartupDetailsComponent }    from './startup-details.component';
import { StartupEditComponent }       from './startup-edit.component';
import { StartupInvestComponent }     from './startup-invest.component';
import { AboutComponent }             from './about.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'dashboard-startup',
    component: DashboardStartupComponent
  },
  {
    path: 'dashboard-investor',
    component: DashboardInvestorComponent
  },
  {
    path: 'startup-list',
    component: StartupListComponent
  },
  {
    path: 'startup-details/:id',
    component: StartupDetailsComponent
  },
  {
    path: 'startup-invest/:id',
    component: StartupInvestComponent
  },
  {
    path: 'startup-edit',
    component: StartupEditComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
