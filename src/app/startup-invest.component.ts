import { Component, OnInit, OnDestroy, HostBinding,
         trigger, transition, animate,
         style, state } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Angular2Apollo } from 'angular2-apollo';
import { Subscription } from 'rxjs/Subscription';

import gql from 'graphql-tag';

const startupQuery = gql`
  query startup ($id: ID!) {
      Startup(id: $id) {
        name
          funds {
            id
            amount
            description
            nbActions
            actions {
              nbAction
            }
            orders(filter:{orderType:"Sale"}) {
              nbAction
              directPrice
              expirationDate
            }
          }
      }
  }
`;

@Component({
  selector: 'startup-invest',
  templateUrl: 'startup-invest.component.html'
})
export class StartupInvestComponent implements OnInit, OnDestroy {

  investorId: string = "cit6f3hg701qk0131hwue6g14"; // Default investor (no login right now)

  id: string;
  loading: boolean = true;
  startup: any;
  startupSub: Subscription;
  currentFund: any;

  constructor(
      private apollo: Angular2Apollo,
      private route: ActivatedRoute,
      private router: Router) {}

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
       this.id = params['id'];
       this.startupSub = this.apollo.watchQuery({
          query: startupQuery,
          variables: { id: this.id },
          pollInterval: 5000,
        }).subscribe(({data, loading}) => {
          this.startup = data.Startup;
          console.log("RESULT", this.startup);
          this.loading = loading;
        });
     });
  }

  ngOnDestroy() {
    this.startupSub.unsubscribe();
  }

  onChange(newValue) {
    if (newValue) {
      this.currentFund = newValue;
    }
  }

  availableToBuy() {
    if (!this.currentFund) {
      return 0;
    }

    let fund = this.searchFund(this.currentFund);

    let total = fund.nbActions;
    let totalSold = 0;

    for (let action of fund.actions) {
       totalSold += action.nbAction;
    }

    return total - totalSold;
  }

  partPrice(fund) {
    return Math.round(fund.amount / fund.nbActions);
  }

  searchFund(id) {
      for (let fund of this.startup.funds) {
         if (fund.id == id) {
           return fund;
         }
      }
      return null;
  }

  currentRetailPrice(fund, currentPrice) {
    let defaultPrice = this.partPrice(fund);

    if (currentPrice > defaultPrice) {
      return '<div class="label label-success"><span style="color:white" class="fa fa-1 fa-arrow-up">&nbsp;</span>' + currentPrice + "$";
    } else if (currentPrice == defaultPrice) {
      return '<div class="label label-default"><span style="color:white" class="fa fa-1 fa-arrow-right">&nbsp;</span>' + currentPrice + "$";
    } else {
      return '<div class="label label-danger"><span style="color:white" class="fa fa-1 fa-arrow-down">&nbsp;</span>' + currentPrice + "$";
    }
  }

  invest(values:any): void {
    let fund = this.searchFund(this.currentFund);

    this.apollo.mutate({
        mutation: gql`
            mutation ($nbAction: Int!, $fundId: ID!, $investorId: ID!) {
              createAction(nbAction: $nbAction, fundId: $fundId, investorId: $investorId) {
                id
              }
            }
        `,
        variables: {
          nbAction: +values.nbPart,
          fundId: fund.id,
          investorId: this.investorId
        },
      }).then(() => {
        let price = this.partPrice(fund);
        alert('Perfect! You are in ! You bought ' + values.nbPart + ' parts for a total price of ' + (price * values.nbPart) + '$');
      });
  }

  hasSellingPart() {
    for (let fund of this.startup.funds) {
      for (let order of fund.orders) {
        return true;
      }
    }
    return false;
  }

}
