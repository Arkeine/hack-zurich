import { Component, OnInit, OnDestroy, HostBinding,
         trigger, transition, animate,
         style, state } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Angular2Apollo } from 'angular2-apollo';
import { Subscription } from 'rxjs/Subscription';


import gql from 'graphql-tag';

const startupQuery = gql`
  query startup ($id: ID!) {
      Startup(id: $id) {
          name
          funds {
            amount
            description
            nbActions
            launchDate
            actions {
              nbAction
              investor {
                id
                name
              }
            }
            orders(filter:{orderType:"Sale"}) {
              directPrice
            }
          }
      }
  }
`;

interface IInvestor {
   name: string;
   numberPart: number;
   funds: string[];
}

@Component({
  selector: 'dashboard-startup',
  templateUrl: 'dashboard-startup.component.html'
})
export class DashboardStartupComponent {

  startupId: string = "cit6f1idi07g80124a6svyamb"; // Default investor (no login right now)
  loading: boolean = true;
  startup: any;
  startupSub: Subscription;
  investors: { [id: string] : IInvestor; } = {};
  totalNumberPart: number;

  constructor(
      private apollo: Angular2Apollo,
      private route: ActivatedRoute,
      private router: Router) {}

  ngOnInit() {
    this.startupSub = this.apollo.watchQuery({
       query: startupQuery,
       variables: {
         id: this.startupId
       },
       pollInterval: 10000,
     }).subscribe(({data, loading}) => {
       this.startup = data.Startup;
       console.log("RESULT: ", this.startup);
       this.computeInvestor();
       this.loading = loading;
     });
  }

  ngOnDestroy() {
    this.startupSub.unsubscribe();
  }

  progression(fund:any) {
    let totalAction = fund.nbActions;
    let totalActionSold = 0;

    for (let action of fund.actions) {
      totalActionSold += action.nbAction;
    }

    return Math.round((totalActionSold / totalAction) * 100);
  }

  currentRetailPrice(fund:any) {
    let numberSale = 0;
    let priceSale = 0;

    for (let order of fund.orders) {
      numberSale += 1;
      priceSale += order.directPrice;
    }

    if (numberSale == 0) {
      return "Currently no sales"
    }

    let currentPrice = Math.round(priceSale / numberSale);
    let defaultPrice = this.partPrice(fund);

    if (currentPrice > defaultPrice) {
      return '<div class="label label-success"><span style="color:white" class="fa fa-1 fa-arrow-up">&nbsp;</span>Retail price : ' + currentPrice + "$";
    } else if (currentPrice == defaultPrice) {
      return '<div class="label label-default"><span style="color:white" class="fa fa-1 fa-arrow-right">&nbsp;</span>Retail price : ' + currentPrice + "$";
    } else {
      return '<div class="label label-danger"><span style="color:white" class="fa fa-1 fa-arrow-down">&nbsp;</span>Retail price : ' + currentPrice + "$";
    }
  }

  partPrice(fund:any) {
    return Math.round(fund.amount / fund.nbActions);
  }

  computeInvestor() {
    this.totalNumberPart = 0;
    for (let fund of this.startup.funds) {
      for (let action of fund.actions) {
        let currentInvestor = this.investors[action.investor.id];
        if (currentInvestor) {
          currentInvestor.numberPart += action.nbAction;
          if (currentInvestor.funds.indexOf(fund.description) < 0) {
            currentInvestor.funds.push(fund.description);
          }
        } else {
          this.investors[action.investor.id] = { name: action.investor.name, numberPart: action.nbAction, funds: [fund.description] };
        }
      }
      this.totalNumberPart += fund.nbActions;
    }
    console.log("Investor: ", this.investors);
  }

  owningPercent(key:string) {
    let percent = this.investors[key].numberPart / this.totalNumberPart;
    return Math.round(percent * 100);
  }

  keys() : Array<string> {
    return Object.keys(this.investors);
  }

}
